# jlog
一个类似于SLF4J或Apache Commons Logging的Java 通用日志接口。

## 简介
一个类似于SLF4J或Apache Commons Logging的通用Java日志接口。它不像LOG4J一样本身就是一种LOG的实现，
各个LOG实现上提供了一种抽象。

像SLF4J一样，它能探测当前Classpath中的所有LOG实现(LogBinder类的实现类)，并选择其中一个最合适的作为默认
的LOG实现。LogFactory加载时会扫描类路径下所有的Log实现(LogBinder接口的实现类)，并从中选择一个合适的实现作为创建
Log的工厂类。它选择各个LOG实现的策略是：
- getWeight
当前Log实现优先级，LogFactory会优先选用高优先级的Log实现。
- isAvailable
当前Log实现是否可用，即其依赖的Jar包是否存在。 

根据这两条规则，你能很方便地编写一个高优先级的Log实现，让JLog优先使用它。

## 下载
RELEASE: [1.0.0-RELEASE](http://git.oschina.net/jiangyc0/jlog/releases)

## 构建
该项目选用Gradle作为构建工具，你能很方便地再几乎所有平台上构建它。
```bash
git clone http://git.oschina.net/jiangyc0/jlog.git

cd jlog
gradlew build
```

## FAQ
```java
public class Test {
    public static void main(String[] args) {
        Log l = LogFactory.getLog(Test.class);
        l.debug("Hello JLog");
        // formatter string
        l.debug("Hello%5s", "JLog");
    }
}
```

## 自定义Log实现
你可以编写自己的Log实现，并让JLog自动加载它。实习自己的Log实现的步骤如下：
- 编写`com.jopenlab.jlog.api.spi.LogBinder`的实现类
- 在`META-INF/services`目录下创建名为`com.jopenlab.jlog.api.spi.LogBinder`的文件
- 编辑文件，内容为自定义的继承于`com.jopenlab.jlog.api.spi.LogBinder`的类的全类名
