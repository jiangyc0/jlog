package com.jopenlab.jlog.log4j;

import com.jopenlab.jlog.api.Log;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * Created by jiangyc on 2016/11/21.
 */
public class Log4jLog implements Log {
    /** the log instance of log4f */
    private Logger logger;

    /**
     * default constructor, using the giving log name
     * @param name the giving log name
     */
    public Log4jLog(String name) {
        logger = Logger.getLogger(name);
    }

    @Override
    public void fatal(String format, Object... args) {
        logger.fatal(String.format(format, args));
    }

    @Override
    public void fatal(Object o, Throwable t) {
        logger.fatal(o, t);
    }

    @Override
    public void fatal(Object o) {
        logger.fatal(o);
    }

    @Override
    public boolean isFatalEnable() {
        return logger.isEnabledFor(Level.FATAL);
    }

    @Override
    public void error(String format, Object... args) {
        error(String.format(format, args));
    }

    @Override
    public void error(Object o, Throwable t) {
        logger.error(o, t);
    }

    @Override
    public void error(Object o) {
        logger.error(o);
    }

    @Override
    public boolean isErrorEnable() {
        return logger.isEnabledFor(Level.ERROR);
    }

    @Override
    public void warn(String format, Object... args) {
        warn(String.format(format, args));
    }

    @Override
    public void warn(Object o, Throwable t) {
        logger.warn(o, t);
    }

    @Override
    public void warn(Object o) {
        logger.warn(o);
    }

    @Override
    public boolean isWarnEnable() {
        return logger.isEnabledFor(Level.WARN);
    }

    @Override
    public void info(String format, Object... args) {
        info(String.format(format, args));
    }

    @Override
    public void info(Object o, Throwable t) {
        logger.info(o, t);
    }

    @Override
    public void info(Object o) {
        logger.info(o);
    }

    @Override
    public boolean isInfoEnable() {
        return logger.isEnabledFor(Level.INFO);
    }

    @Override
    public void debug(String format, Object... args) {
        debug(String.format(format, args));
    }

    @Override
    public void debug(Object o, Throwable t) {
        logger.debug(o, t);
    }

    @Override
    public void debug(Object o) {
        logger.debug(o);
    }

    @Override
    public boolean isDebugEnable() {
        return logger.isEnabledFor(Level.DEBUG);
    }

    @Override
    public void trace(String format, Object... args) {
        trace(String.format(format, args));
    }

    @Override
    public void trace(Object o, Throwable t) {
        logger.trace(o, t);
    }

    @Override
    public void trace(Object o) {
        logger.trace(o);
    }

    @Override
    public boolean isTraceEnable() {
        return logger.isEnabledFor(Level.TRACE);
    }
}
