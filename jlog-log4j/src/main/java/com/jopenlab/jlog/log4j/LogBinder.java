package com.jopenlab.jlog.log4j;

import com.jopenlab.jlog.api.Log;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by jiangyc on 2016/11/21.
 */
public class LogBinder implements com.jopenlab.jlog.api.spi.LogBinder {
    /**
     * A collection of log implementation classes
     */
    private static Map<String, Log> logMap = new ConcurrentHashMap<>();

    @Override
    public Log getLog(String name) {
        Log log = logMap.get(name);

        if (log == null) {
            log = new Log4jLog(name);
            logMap.put(name, log);
        }

        return log;
    }

    @Override
    public int getWeight() {
        return 0;
    }

    @Override
    public boolean isAvailable() {
        try {
            Class c = Class.forName("org.apache.log4j.Logger");
            return c != null;
        } catch (ClassNotFoundException e) {
            return false;
        }
    }
}
