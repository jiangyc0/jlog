package com.jopenlab.jlog.log4j;

import com.jopenlab.jlog.api.Log;
import com.jopenlab.jlog.api.LogFactory;
import org.junit.Test;

/**
 * Created by jiangyc on 2016/11/21.
 */
public class LogFactoryTest {

    @Test
    public void test() {
        Log log = LogFactory.getLog(this.getClass());
        log.fatal("fatel msg...");
        log.error("error msg...");
        log.warn("warn msg...");
        log.info("info msg...");
        log.debug("debug msg...");
        log.trace("trace msg...");
    }
}
